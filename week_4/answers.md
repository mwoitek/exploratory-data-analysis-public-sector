Week 4 Assignment
=================

**What is the dataset (2-3 sentences)?**

For this assignment, I have used data from the Brazilian government.
Specifically, I have considered a dataset related to intentional violent
deaths in Brazil and in each of its states. This dataset contains death
figures and rates for the period from 2011 to 2021.

Source: [Fórum Brasileiro de Segurança Pública](http://forumseguranca.org.br:3838/)

**What is the question you want to answer with your visualization?**

Actually, I was trying to answer a few different questions. My general
question was the following: how has the amount of violent death in Brazil
changed over the past decade or so? I was especially interested in the
period starting in 2019. That year, a conservative politician took office
as president. Since he was elected in part due to his "tough on crime"
rhetoric, I wanted to know if this president was able to significantly
reduce the number of violent deaths. Also, from 2020 onwards, we can see
the impact of COVID on crime. It was reasonable to expect that murder rates
would be lower during the pandemic. Was this assumption correct? If so, how
much lower were those rates?

Another question I was interested in was: how do these statistics on
violent deaths change from state to state? In case you didn't know: the
Brazilian states in the south and southeast are the richest and most
developed. Then I wanted to see if the death rates in those states were
considerably different from the ones in the rest of the country.

**What is your approach to cleaning and visualizing the data, including
your approach to making the visual easy to read and understand (a few
sentences)?**

Fortunately, I didn't need to do any data cleaning. The data I used was
clean and complete.

When it comes to data visualization, my approach is essentially Tufte's
approach. Like him, I believe that clarity comes from minimalism. So I try
to remove from my charts everything that doesn't add meaning.

**What is your source code for creating the visualization (paste it)?**

I decided not to paste my code here. If you want to see my code, please have a
look at this [Gitlab
repository](https://gitlab.com/mwoitek/exploratory-data-analysis-public-sector/-/tree/master/week_4)
I created. There you will find all the files I used to complete this
assignment. I think this is better.

---

Yes, share.

I really don't care about taking credit. If you insist, just use my name:
Marcio Woitek.
